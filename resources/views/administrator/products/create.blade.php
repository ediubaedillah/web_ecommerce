@extends('layouts.sb_admin_app')
@section('title')
    Tambah Produk
@endsection
@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tambah Produk</h1>
    </div>
    <form action="" class="form-horizontal form-general">
        <div class="row">
            <div class="col-lg-12">

                <div class="card shadow mb-4">

                    <div class="card-body">
                        <div class="title-form">
                            <h2>Informasi Produk</h2>
                        </div>

                        <div class="form-group">
                            <label for="">Nama Produk</label>
                            <input type="text" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Deskripsi</label>
                            <textarea name="description" id="description" cols="30" rows="4" class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="">Kategori</label>
                            <select name="category" id="category" class="form-control">
                                <option value="">Pilih Kategori</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Merek</label>
                            <select name="merk" id="merk" class="form-control">
                                <option value="">Pilih Merek</option>
                            </select>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow mb-4">

                    <div class="card-body">
                        <div class="title-form">
                            <h2>Informasi Penjualan</h2>
                        </div>

                        <div class="form-group">
                            <label for="">Harga</label>
                            <input type="text" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Stok</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow mb-4">

                    <div class="card-body">
                        <div class="title-form">
                            <h2>Pengaturan Media</h2>
                        </div>

                        <div class="form-group">
                            <label for="">Foto Produk</label>
                            <input type="file" class="form-control">
                            <input type="file" class="form-control">
                            <input type="file" class="form-control">
                            <input type="file" class="form-control">
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <button type="submit" class="btn btn-primary btn-flat">Simpan & Tampilkan</button>
                <button type="submit" class="btn btn-primary btn-flat">Simpan & Arsipkan</button>
                <button type="submit" class="btn btn-danger btn-flat">Batal</button>
            </div>
        </div>
    </form>
@endsection
